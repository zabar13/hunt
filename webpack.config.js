var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: ['./src/index.js']
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test:/\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|gif|mp3|wav)$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                    }
                  }
                ]
              }
        ]
    },
    plugins:[new HtmlWebpackPlugin({
        title: 'My learn project',
        template: 'src/index.html'
    })]

};