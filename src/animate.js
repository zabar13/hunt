import './main.css';
import $ from 'jquery';
import {playShot} from './audio';


var speedFactor = 1;
var score = 0;
var startGame = false;


export function doGameRun(run) {
    run ? startGame = true : startGame = false;
};

export function totalScore() {
    alert('Score ' + score + ' in level ' + speedFactor);
}

function makeNewPos(){
    
    var posX = $(window).height() - 150;
    var posY = $(window).width() - 150;
    
    var newPosX = Math.floor(Math.random() * posX);
    var newPosY = Math.floor(Math.random() * posY);
    
    return [newPosX,newPosY];    
    
}

export function animateDiv(){
    var newPos = makeNewPos();
    var speed = calcSpeed();
    $('.circle').animate({ top: newPos[0], left: newPos[1] }, speed, function(){
      animateDiv(); 
    });
    
};

function calcSpeed() {
    var speed = Math.ceil(3000/speedFactor);
    return speed;

};

$(".speedPlus").click(function(){
    startGame ? speedFactor : speedPlus();
});

function speedPlus() {
    speedFactor >= 9 ? speedFactor = 9 : speedFactor ++;
    $('#speed').text(speedFactor);
}

$(".speedMinus").click(function(){
    startGame ? speedFactor : speedMinus();
});

function speedMinus() {
    speedFactor <= 1 ? speedFactor = 1 : speedFactor --;
    $('#speed').text(speedFactor);
    
};



export function hit() {
    startGame  ? score++ : score;
    $('#point').text(score);
    };
 