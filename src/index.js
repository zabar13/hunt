import './main.css';
import $ from 'jquery';
import {startCount, stopCount} from "./timer";
import {animateDiv, doGameRun, hit} from './animate';
import {playShot, playImperial, playWelcome, playBoba} from './audio';


$(document).ready( function() {
    $('#pause').hide();
    $('#resume').hide();
    $('#reset').hide();  
});


$('#pause').click( function() {
    doGameRun(false);
    stopCount();
    playBoba();
    playImperial(false);
    playWelcome(false);
    $(".circle" ).stop();
    $('#pause').hide();
    $('#resume').show(); 
});

$('#resume').click( function() {
  doGameRun(true);
  startCount();
  animateDiv();
  //playBoba();
  playImperial(true);
  playWelcome(true);
  $('#pause').show();
  $('#resume').hide();  
});

$(".start").click(function(){
    doGameRun(true);
    playImperial(true);
    playWelcome(true);
    startCount();
    animateDiv();
    $('.start').hide();
    $('#pause').show();
    $('#reset').show();
    });

$('#reset').click(function() {
    location.reload();
    $('.start').show();
});   


$('.circle').click(function() {
        hit();
        playShot();
        
});







