import './main.css';
import $ from 'jquery';
import {totalScore, doGameRun} from './animate';
import {playImperial} from './audio';

    var timeToEnd = 20;  
    var timer;
    var timer_is_on = 0;
    
    function timedCount() {
        $('#timer').text(timeToEnd );
        timer = setTimeout(function(){ timedCount() }, 1000);
        timeToEnd < 0 ? endGame() : timeToEnd--; 
    }

    function endGame() {
        $('#timer').text('0')
        $( ".circle" ).stop();
        doGameRun(false);
        playImperial(false);
        $('#pause').hide();
        $('#resume').hide();
        $('#reset').show();
        totalScore();
        stopCount();
         }
    
    export function startCount() {
        if (!timer_is_on) {
            timer_is_on = 1;
            timedCount();
        }
    }
    
    export function stopCount() {
        clearTimeout(timer);
        timer_is_on = 0;
    }

