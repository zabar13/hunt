import './main.css';
import $ from 'jquery';

var force = new Audio('http://www.galaxyfaraway.com/Sounds/FATHER.WAV');
var welcome = new Audio('http://www.galaxyfaraway.com/Sounds/Welcome.wav')
var imperial = new Audio('http://mattersofgrey.com/audio/imperialmarch.mp3');
var shot = new Audio('http://www.galaxyfaraway.com/Sounds/LAZER.WAV');
var boba = new Audio('http://www.galaxyfaraway.com/Sounds/ASYUWISH.WAV');




export function playShot() {
    shot.play();
};

export function playBoba() {
    boba.play();
};


export function playImperial(x) {
   x ? imperial.play() : imperial.pause()
};

export function playWelcome(x) {
    x ? welcome.play() : welcome.pause()
 };